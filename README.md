Todo app

Tech stack:

-   pnpm package manager
-   monorepo strategy
-   typescript
-   react
-   mantine (components library & hooks)
-   jotai (global state manager)
-   zod (schema)
-   express

Key performance points:

-   text inputs are decoupled from state
-   global state management rerenders only relevant components
-   hover css used to hide/show components without changing state

Drawbacks:

-   while the app is responsive - due to hover showing buttons makes it unable to edit/delete todos while in mobile as hover is not mobile-friendly
