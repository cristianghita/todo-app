import express, { Response } from 'express';
import cors from 'cors';
import { TTodo, zTodo } from '@ghita/shared/models/todo';
import { ApiRoutes } from '@ghita/shared/models/api';

const app = express();
const port = 3000;

const todos: Record<string, TTodo[]> = {};

app.use(cors());
app.use(express.json());

app.get('/hb', (_req, res) => {
    res.send('The heart beats strong!');
});

app.get<{}, TTodo[]>(ApiRoutes.todos, (req, res: Response<TTodo[]>) => {
    const userId = req.headers['userid'] as string | undefined;
    if (!userId) throw new Error('no userId provided');
    if (!todos[userId]) todos[userId] = [];
    res.json(todos[userId]);
});

app.post<{}, TTodo[], TTodo>(
    ApiRoutes.addTodo,
    (req, res: Response<TTodo[]>) => {
        const userId = req.headers['userid'] as string | undefined;
        if (!userId) throw new Error('no userId provided');
        if (!todos[userId]) todos[userId] = [];
        zTodo.parse(req.body);
        todos[userId].push(req.body);
        res.json(todos[userId]);
    }
);

app.delete<{}, TTodo[], TTodo>(
    ApiRoutes.deleteTodo,
    (req, res: Response<TTodo[]>) => {
        const userId = req.headers['userid'] as string | undefined;
        if (!userId) throw new Error('no userId provided');
        if (!todos[userId]) todos[userId] = [];
        const todo = zTodo.parse(req.body);
        todos[userId] = todos[userId].filter((t) => t.id !== todo.id);
        res.json(todos[userId]);
    }
);

app.put<{}, TTodo[], TTodo>(
    ApiRoutes.editTodo,
    (req, res: Response<TTodo[]>) => {
        const userId = req.headers['userid'] as string | undefined;
        if (!userId) throw new Error('no userId provided');
        if (!todos[userId]) todos[userId] = [];
        const todo = zTodo.parse(req.body);
        todos[userId] = todos[userId].map((t) => (t.id === todo.id ? todo : t));
        res.json(todos[userId]);
    }
);

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
