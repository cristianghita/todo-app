import { atom } from 'jotai';
import { api } from '../utils/api.util';
import { todosAtom } from './todos.atoms';
import { store } from './store';

export const userIdAtom = atom('');

export const setUserIdAtom = atom<null, [string], void>(
    null,
    async (_, set, userId) => {
        set(userIdAtom, userId);
        store.set(userIdAtom, userId);
        const todos = await api.getTodos();
        set(todosAtom, todos);
    }
);
