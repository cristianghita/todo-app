import { atom } from 'jotai';
import { TTodo } from '@ghita/shared/models/todo';
import { api } from '../utils/api.util';

export const todosAtom = atom<TTodo[]>([]);

export const addTodoAtom = atom<null, [TTodo], void>(
    null,
    async (get, set, newTodo) => {
        const todos = get(todosAtom);
        set(todosAtom, [...todos, newTodo]);
        const newTodos = await api.addTodo(newTodo);
        set(todosAtom, newTodos);
    }
);

export const editTodoAtom = atom<null, [TTodo, number], void>(
    null,
    async (get, set, newTodo, index) => {
        const todos = [...get(todosAtom)];
        todos[index] = newTodo;
        set(todosAtom, todos);
        const newTodos = await api.editTodo(newTodo);
        set(todosAtom, newTodos);
    }
);

export const deleteTodoAtom = atom<null, [TTodo], void>(
    null,
    async (get, set, todo) => {
        const todos = [...get(todosAtom)];
        todos.filter((el) => el.id !== todo.id);
        set(todosAtom, todos);
        const newTodos = await api.deleteTodo(todo);
        set(todosAtom, newTodos);
    }
);
