export type TLocalStorage = {
    userId: string;
};

export type TLocalStorageKey = keyof TLocalStorage;
