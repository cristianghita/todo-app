import { FC, useEffect, useRef } from 'react';
import { Shell } from '../components/Shell';
import { Flex, Paper, Text } from '@mantine/core';
import { IconNotes } from '@tabler/icons-react';
import { TodoInput } from '../components/TodoInput';
import { TodoList } from '../components/TodoList';
import { ls } from '../utils/localStorage.util';
import { randomId } from '@mantine/hooks';
import { useAtom } from 'jotai';
import { setUserIdAtom } from '../atoms/user.atoms';
import { useMobile } from '../hooks/useMobile';

export const TodoPage: FC = () => {
    const didSetUserId = useRef(false);
    const [, setUserId] = useAtom(setUserIdAtom);
    const isMobile = useMobile();

    useEffect(() => {
        if (didSetUserId.current) return;
        let userId = ls.getItem('userId');
        if (!userId) {
            userId = randomId();
            ls.setItem('userId', userId);
        }

        setUserId(userId);
        didSetUserId.current = true;
    }, []);

    return (
        <Shell>
            <Paper shadow="xl" h="97vh" p={isMobile ? 'xs' : 'md'}>
                <Flex direction="column" gap="xs">
                    <Flex gap="xs" align="center">
                        <IconNotes size={30} stroke={1.5} />
                        <Text size="xl" fw={700}>
                            Todo App
                        </Text>
                    </Flex>
                    <TodoInput />
                    <Text size="md" ml="xs">
                        My todos
                    </Text>
                    <TodoList />
                </Flex>
            </Paper>
        </Shell>
    );
};
