import { TLocalStorageKey, TLocalStorage } from '../models/localStorage.model';

const setItem = <TKey extends TLocalStorageKey, TValue = TLocalStorage[TKey]>(
    key: TKey,
    value: TValue
) => {
    localStorage.setItem(key, JSON.stringify(value));
};

const getItem = <TKey extends TLocalStorageKey, TValue = TLocalStorage[TKey]>(
    key: TKey
): TValue | null => {
    const item = localStorage.getItem(key);
    if (!item) return null;
    try {
        return JSON.parse(item) as TValue;
    } catch {
        const floatValue = parseFloat(item);
        return (Number.isNaN(floatValue) ? item : floatValue) as TValue;
    }
};

export const ls = {
    setItem,
    getItem,
};
