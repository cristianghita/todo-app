import { TTodo, zTodo } from '@ghita/shared/models/todo';
import { ApiRoutes } from '@ghita/shared/models/api';
import { userIdAtom } from '../atoms/user.atoms';
import { store } from '../atoms/store';

const buildUrl = (route: keyof typeof ApiRoutes) =>
    `http://localhost:3000${ApiRoutes[route]}`;

const getTodos = async () => {
    const userId = store.get(userIdAtom);
    const fetchResult = await fetch(buildUrl('todos'), {
        method: 'GET',
        headers: { userId },
    });
    const todos = await fetchResult.json();
    todos.map(zTodo.parse);
    return todos as TTodo[];
};

const addTodo = async (todo: TTodo) => {
    const userId = store.get(userIdAtom);
    const fetchResult = await fetch(buildUrl('addTodo'), {
        method: 'POST',
        headers: { userId, 'Content-Type': 'application/json' },
        body: JSON.stringify(todo),
    });
    const todos = await fetchResult.json();
    todos.map(zTodo.parse);
    return todos as TTodo[];
};

const deleteTodo = async (todo: TTodo) => {
    const userId = store.get(userIdAtom);
    const fetchResult = await fetch(buildUrl('deleteTodo'), {
        method: 'DELETE',
        headers: { userId, 'Content-Type': 'application/json' },
        body: JSON.stringify(todo),
    });
    const todos = await fetchResult.json();
    todos.map(zTodo.parse);
    return todos as TTodo[];
};

const editTodo = async (todo: TTodo) => {
    const userId = store.get(userIdAtom);
    const fetchResult = await fetch(buildUrl('editTodo'), {
        method: 'PUT',
        headers: { userId, 'Content-Type': 'application/json' },
        body: JSON.stringify(todo),
    });
    const todos = await fetchResult.json();
    todos.map(zTodo.parse);
    return todos as TTodo[];
};

export const api = {
    addTodo,
    deleteTodo,
    editTodo,
    getTodos,
} as const;
