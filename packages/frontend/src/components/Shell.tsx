import { AppShell, Container } from '@mantine/core';
import { FC } from 'react';
import { useMobile } from '../hooks/useMobile';

type TShellProps = {
    children: React.ReactNode;
};

export const Shell: FC<TShellProps> = ({ children }) => {
    const isMobile = useMobile();
    return (
        <AppShell padding={isMobile ? 'xs' : 'md'}>
            <AppShell.Main>
                <Container fluid maw={800} px={isMobile ? 0 : 'sm'}>
                    {children}
                </Container>
            </AppShell.Main>
        </AppShell>
    );
};
