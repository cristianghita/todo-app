import { ActionIcon, MantineColor, MantineSize } from '@mantine/core';
import {
    IconDeviceFloppy,
    IconProps,
    IconCancel,
    IconEdit,
} from '@tabler/icons-react';
import { FC } from 'react';

type TTodoInputActionProps = {
    action: 'save' | 'edit' | 'cancel';
    onAction: () => void;
    size?: (string & {}) | MantineSize;
};

const actionMap: Record<
    TTodoInputActionProps['action'],
    { color: MantineColor; Icon: FC<IconProps> }
> = {
    cancel: {
        color: 'red',
        Icon: IconCancel,
    },
    edit: {
        color: 'yellow',
        Icon: IconEdit,
    },
    save: {
        color: 'blue',
        Icon: IconDeviceFloppy,
    },
};

export const TodoInputAction: FC<TTodoInputActionProps> = ({
    action,
    onAction,
    size,
}) => {
    const { color, Icon } = actionMap[action];
    return (
        <ActionIcon
            variant="filled"
            onClick={onAction}
            color={color}
            size={size === 'xs' ? 'sm' : 'md'}
        >
            <Icon
                stroke={1.5}
                style={{
                    height: size === 'xs' ? '70%' : '80%',
                    width: size === 'xs' ? '70%' : '80%',
                }}
            />
        </ActionIcon>
    );
};
