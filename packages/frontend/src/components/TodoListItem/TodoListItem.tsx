import { TTodo } from '@ghita/shared/models/todo';
import { ActionIcon, Flex, Text, Tooltip } from '@mantine/core';
import { FC, useState } from 'react';
import classes from './TodoListItem.module.css';
import { IconEdit, IconTrash } from '@tabler/icons-react';
import { TodoInputBase } from '../TodoInputBase';
import { useAtom } from 'jotai';
import { deleteTodoAtom, editTodoAtom } from '../../atoms/todos.atoms';

type TTodoListItemProps = {
    todo: TTodo;
    index: number;
};

export const TodoListItem: FC<TTodoListItemProps> = ({ todo, index }) => {
    const { id, value } = todo;
    const [, editTodo] = useAtom(editTodoAtom);
    const [, deleteTodo] = useAtom(deleteTodoAtom);
    const [edit, setEdit] = useState(false);

    const handleEditClick = () => setEdit((prev) => !prev);

    const handleEditValue = (partialTodo: Partial<TTodo>) => {
        editTodo({ id, value, ...partialTodo }, index);
        handleEditClick();
    };

    const handleDelete = () => {
        deleteTodo(todo);
    };

    return (
        <Flex gap="xs" key={id} className={classes.container} direction="row">
            {edit ? (
                <TodoInputBase
                    size="xs"
                    w={395}
                    leftSection={<div>{index + 1}.</div>}
                    defaultValue={value}
                    onEdit={handleEditValue}
                    onCancel={handleEditClick}
                />
            ) : (
                <Text size="lg">
                    {index + 1}. {value}
                </Text>
            )}

            {!edit && (
                <Flex
                    className={classes.actionsContainer}
                    gap="5px"
                    align="center"
                >
                    <Tooltip label="Edit">
                        <ActionIcon
                            variant="filled"
                            size="sm"
                            color="yellow"
                            onClick={handleEditClick}
                        >
                            <IconEdit
                                style={{ height: '80%', width: '80%' }}
                                stroke={1.5}
                            />
                        </ActionIcon>
                    </Tooltip>
                    <Tooltip label="Delete">
                        <ActionIcon
                            variant="filled"
                            size="sm"
                            color="red"
                            onClick={handleDelete}
                        >
                            <IconTrash
                                style={{ height: '80%', width: '80%' }}
                                stroke={1.5}
                            />
                        </ActionIcon>
                    </Tooltip>
                </Flex>
            )}
        </Flex>
    );
};
