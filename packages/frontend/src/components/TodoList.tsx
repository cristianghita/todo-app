import { Flex } from '@mantine/core';
import { useAtom } from 'jotai';
import { FC, memo } from 'react';
import { todosAtom } from '../atoms/todos.atoms';
import { TodoListItem } from './TodoListItem/TodoListItem';

export const TodoList: FC = memo(function TodoList() {
    const [todos] = useAtom(todosAtom);

    return (
        <Flex direction="column" ml="md">
            {todos.map((el, i) => (
                <TodoListItem key={el.id} todo={el} index={i} />
            ))}
        </Flex>
    );
});
