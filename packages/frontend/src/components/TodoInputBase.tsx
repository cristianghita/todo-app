import { TTodo } from '@ghita/shared/models/todo';
import { Flex, TextInput, TextInputProps } from '@mantine/core';
import { mergeRefs, randomId } from '@mantine/hooks';
import { FC, ReactNode, forwardRef, useMemo, useRef, useState } from 'react';
import { TodoInputAction } from './TodoInputAction';

type TTodoInputBaseProps = TextInputProps & {
    onSave?: (todo: TTodo) => void;
    onEdit?: (todo: Partial<TTodo>) => void;
    onCancel?: () => void;
};

export const TodoInputBase: FC<TTodoInputBaseProps> = forwardRef<
    HTMLInputElement,
    TTodoInputBaseProps
>(({ onSave, onEdit, onCancel, onKeyDown, ...props }, ref) => {
    const inputRef = useRef<HTMLInputElement>(null);
    const [error, setError] = useState('');

    const validate = (callback: (validatedValue: string) => void) => {
        if (!inputRef.current) return;
        const value = inputRef.current.value;
        if (!value.trim()) {
            setError("Value can't be empty");
        } else {
            callback(value);
        }

        inputRef.current.focus();
    };

    const handleSave = () => {
        if (!inputRef.current || !onSave) return;

        validate((value) => {
            onSave({ id: randomId(), value });

            if (!inputRef.current) return;
            inputRef.current.value = '';
        });
    };

    const handleEdit = () => {
        if (!inputRef.current || !onEdit) return;

        validate((value) => {
            onEdit({ value });
        });
    };

    const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        setError('');
        event.key === 'Enter' && handleSave();
        onKeyDown?.(event);
    };

    const rightSection = useMemo(() => {
        const actionIcons: ReactNode[] = [];
        onSave &&
            actionIcons.push(
                <TodoInputAction
                    key="save"
                    action="save"
                    onAction={handleSave}
                    size={props.size}
                />
            );
        onEdit &&
            actionIcons.push(
                <TodoInputAction
                    key="edit"
                    action="edit"
                    onAction={handleEdit}
                    size={props.size}
                />
            );
        onCancel &&
            actionIcons.push(
                <TodoInputAction
                    key="cancel"
                    action="cancel"
                    onAction={onCancel}
                    size={props.size}
                />
            );
        return {
            value: <Flex gap="5px">{actionIcons.map((el) => el)}</Flex>,
            length: actionIcons.length,
        };
    }, [onSave, onEdit, onCancel]);

    return (
        <TextInput
            ref={mergeRefs(ref, inputRef)}
            onKeyDown={handleKeyDown}
            {...props}
            error={error || props.error}
            rightSection={rightSection.value}
            rightSectionWidth={rightSection.length * 33}
        />
    );
});
