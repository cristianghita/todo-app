import { useAtom } from 'jotai';
import { FC, memo } from 'react';
import { addTodoAtom } from '../atoms/todos.atoms';
import { TodoInputBase } from './TodoInputBase';

export const TodoInput: FC = memo(function TodoInput() {
    const [, addTodo] = useAtom(addTodoAtom);

    return (
        <TodoInputBase
            label="Add new todo"
            maw={400}
            ml="xs"
            placeholder="Do the dishes.."
            size="md"
            onSave={addTodo}
        />
    );
});
