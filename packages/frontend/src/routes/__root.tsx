import { createRootRoute, Outlet } from '@tanstack/react-router';
import { createTheme, MantineProvider } from '@mantine/core';
import { TodoPage } from '../pages/TodoPage';

const theme = createTheme({});

export const Route = createRootRoute({
    component: () => (
        <MantineProvider theme={theme}>
            <Outlet />
        </MantineProvider>
    ),
});
