import { createLazyFileRoute } from '@tanstack/react-router';
import { TodoPage } from '../pages/TodoPage';

export const Route = createLazyFileRoute('/')({
    component: TodoPage,
});
