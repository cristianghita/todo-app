import { z } from 'zod';

export const zTodo = z.object({
    value: z.string().min(1),
    id: z.string().min(1),
});

export type TTodo = z.infer<typeof zTodo>;
