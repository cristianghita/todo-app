export const ApiRoutes = {
    addTodo: '/add-todo',
    deleteTodo: '/delete-todo',
    editTodo: '/edit-todo',
    todos: '/todos',
} as const;
